import traceback

import flask

from flask import request, make_response, jsonify
from flask_sqlalchemy import SQLAlchemy
from werkzeug.exceptions import HTTPException

from config import configure_app
from lib.exceptions import InvalidUsage


app = flask.Flask(__name__)
configure_app(app)

db = SQLAlchemy(app)


# -----------------------------------------------------------------------------
# API BLUEPRINT
# -----------------------------------------------------------------------------
from api.api import api


@api.errorhandler(InvalidUsage)
def api_error_handler(error: InvalidUsage):
    return make_response(jsonify(error.to_dict()), error.code)


@app.errorhandler(400)
@app.errorhandler(404)
def api_http_error_handler(error: HTTPException):
    if request.path.startswith('/api/'):
        er = InvalidUsage(error.description, error.code)
        return make_response(jsonify(er.to_dict()), er.code)


@app.errorhandler(Exception)
def api_common_error_handler(error: Exception):
    if request.path.startswith('/api/'):
        er = InvalidUsage("Internal server error", 500)
        app.logger.error(error)
        app.logger.error(traceback.extract_tb(error.__traceback__))
        return make_response(jsonify(er.to_dict()), er.code)

app.register_blueprint(api)
# -----------------------------------------------------------------------------
# API BLUEPRINT
# -----------------------------------------------------------------------------





