from flask import Blueprint, jsonify, make_response, request
from sqlalchemy.orm import Query

from app import db
from models import Contact
from lib.exceptions import InvalidUsage

api = Blueprint('api', 'api', url_prefix='/api')

contact_book = {'items': [{'contact_id': 1, 'contact1': 'Andrey'},
                          {'contact_id': 2, 'contact2': 'Sergey'},
                          {'contact_id': 3, 'contact3': 'Evrey'}]}
contact_book_ids = [1, 2, 3]


@api.route('/contacts/', methods=['GET'], defaults={'contact_id': None})
@api.route('/contacts/<int:contact_id>', methods=['GET'])
def show_contacts(contact_id: int):
    """

    :param contact_id:
    :return:
    """
    _cq: Query = Contact.query
    if contact_id is not None:
        _c: Contact = _cq.get(contact_id)
        result = _c.to_dict() if _c else {}
    else:
        _cs = _cq.filter().all()
        result = {'items': [_c.to_dict() for _c in _cs],
                  'count': len(_cs)}
    return make_response(jsonify(result), 200)


@api.route('/contacts/<int:contact_id>', methods=['PUT', 'PATCH'])
def edit_contacts(contact_id: int):
    """

    :param contact_id:
    """
    data = request.get_json()
    if data:
        _c = Contact.query.get(contact_id)
        _cf = Contact.query.filter(Contact.id == contact_id)
        if _c:
            _cf.update(data)
            db.session.commit()
            return make_response(jsonify(_c.to_dict()), 200)
        else:
            raise InvalidUsage(f'Contact <id:{contact_id}> is not found', 404)
    else:
        raise InvalidUsage('Where is no request body')


@api.route('/contacts/', methods=['DELETE'], defaults={'contact_id': None})
@api.route('/contacts/<int:contact_id>', methods=['DELETE'])
def delete_contacts(contact_id: int):
    """

    :param contact_id:
    """

    _cq: Query = Contact.query
    if contact_id is not None:
        _c: Contact = _cq.get(contact_id)
        _cf = _cq.filter(Contact.id == contact_id)
        if _c:
            result = make_response(
                jsonify({'successful': bool(_cf.delete('evaluate')),
                         'message': f'Contact <id:{contact_id}>'}), 200)
        else:
            raise InvalidUsage(f'Contact <id: {contact_id}> not found!', 404)
    else:
        ids = map(lambda c: str(c.id), _cq.all())
        result = make_response(
            jsonify({'successful': bool(_cq.delete('evaluate')),
                     'message': f'Contact <ids:[{",".join(ids)}]>'}), 200)
    db.session.commit()
    return result


@api.route('/contacts/', methods=['POST'])
def create_contacts():
    data = request.get_json()
    if data:
        _c = Contact(**data)
        db.session.add(_c)
        db.session.commit()
        return make_response(jsonify(_c.to_dict()), 201)
    else:
        raise InvalidUsage('Where is no request body')

