To create and fill in data base use create_db.py script

```bash 
python3.6 create_db.py
```

To **get** contact with **id=1**
```bash 
curl -X 'GET' http://127.0.0.1:6123/api/contacts/1

```
```json
{
  "created": "Sat, 13 Oct 2018 12:55:38 GMT", 
  "email": "news@email.ru", 
  "first_name": "Ivan1_new", 
  "id": 1, 
  "last_name": "Pirozhkov2_new", 
  "phone": "8960"
}
```


To **get whole list** of contacts
```bash 
curl -X 'GET' http://127.0.0.1:6123/api/contacts/

```

```json
{
  "count": 10, 
  "items": [
    {
      "created": "Sat, 13 Oct 2018 12:55:38 GMT", 
      "email": "news@email.ru", 
      "first_name": "Ivan1_new", 
      "id": 1, 
      "last_name": "Pirozhkov2_new", 
      "phone": "8960"
    }, 
    {
      "created": "Sat, 13 Oct 2018 12:55:38 GMT", 
      "email": "email_2@email.com", 
      "first_name": "Name_2", 
      "id": 2, 
      "last_name": "Last Name", 
      "phone": "89601865985"
    },
    {...}
  ]
}

```


To **delete** contact with **id=1**
```bash 
curl -X 'DELETE' http://127.0.0.1:6123/api/contacts/1

```
```json
{
  "message": "Contact <id:1>", 
  "successful": true
}

```

To **delete all** contacts

```bash 
curl -X 'DELETE' http://127.0.0.1:6123/api/contacts/
```
```json
{
  "message": "Contact <ids:[2,3,4,5,6,7,8,9,10]>", 
  "successful": true
}
```

To **update** contact with **id=2** with PUT method
```bash
curl -X PUT -H "Content-Type: application/json" -d '{"first_name":"new_first_name"}' "http://127.0.0.1:6123/api/contacts/2"
```
```json
{
  "created": "Sun, 14 Oct 2018 09:33:27 GMT", 
  "email": "email_2@email.com", 
  "first_name": "new_first_name", 
  "id": 2, 
  "last_name": "Last Name", 
  "phone": "89601865985"
}
```

To **create new** contact use POST method:
 ```bash
curl -X POST -H "Content-Type: application/json" -d '{"first_name":"Ivan", "last_name": "Pirozhkov", "email": "email_ivan@email.com"}' "http://127.0.0.1:6123/api/contacts/"
```
```json
{
  "created": "Sun, 14 Oct 2018 09:30:08 GMT", 
  "email": "email_ivan@email.com", 
  "first_name": "Ivan", 
  "id": 11, 
  "last_name": "Pirozhkov", 
  "phone": null
}
```

