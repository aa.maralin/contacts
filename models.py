import re
from datetime import datetime
from sqlalchemy.orm import validates
from lib.exceptions import ApiModelValidation

from app import db


class Contact(db.Model):
    """

    """
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(15), unique=False, nullable=False)
    last_name = db.Column(db.String(30), unique=False, nullable=False)
    phone = db.Column(db.String(20), unique=True, nullable=True)
    skype = db.Column(db.String(120), unique=True, nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.now())

    def __repr__(self):
        return f'<Contact id:{self.id},{self.last_name} {self.first_name}>'

    def to_dict(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'phone': self.phone,
            'email': self.email,
            'created': self.created
        }

    @validates('email')
    def validate_email(self, key, email):
        if not email:
            raise ApiModelValidation('Email is required.')

        if Contact.query.filter(Contact.email == email).first():
            raise ApiModelValidation(f'Email <{email}> is already in use.')

        if len(email) < 5 or len(email) > 120:
            raise ApiModelValidation('Email must be between 5 and 120 characters')

        _pattern = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
        if not re.match(_pattern, email):
            raise ApiModelValidation(f'Email <{email}> does not match email '
                                 f'pattern.')
        return email

    @validates('phone')
    def validate_phone(self, key, phone):
        if Contact.query.filter(Contact.phone == phone).first():
            raise ApiModelValidation(f'Phone <{phone}> is already in use.')

        if len(phone) < 5 or len(phone) > 20:
            raise ApiModelValidation(f'Phone <{phone}> must be between 5 and '
                                     f'20 characters')
        return phone

    #todo
    @validates('first_name')
    def validate_first_name(self, key, fname):
        return fname


    #todo
    @validates('last_name')
    def validate_last_name(self, key, lname):
        return lname


    #todo
    @validates('skype')
    def validate_skype(self, key, skype):
        return skype