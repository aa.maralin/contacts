from pathlib import Path
from app import db
from models import Contact


if __name__ == '__main__':
    _path = Path('contact_book.db')
    if _path.exists():
        _path.unlink()

    db.create_all()

    for i in range(1, 11):
        c = Contact(first_name=f'Name_{i}',
                    last_name=f'Last Name',
                    email=f'email_{i}@email.com',
                    phone=str(89601865983+i),
                    skype=f'skype_{i}')
        db.session.add(c)

    db.session.commit()

