from werkzeug.exceptions import HTTPException


class InvalidUsage(HTTPException):
    code = 400

    def __init__(self, description, code=None):
        HTTPException.__init__(self)
        self.description = description
        if code is not None:
            self.code = code

    def to_dict(self):
        return {'description': self.description,
                'http_error': self.name,
                'http_code': self.code}


class ApiModelValidation(InvalidUsage):
    pass
